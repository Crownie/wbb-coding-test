# We Build Bot Coding Test

This is a submission of WBB coding test. 

## Installation

Use npm or yarn

```bash
yarn install
```

## Part 1

```bash
yarn run generate-name-count
```

Parses the files provided in folder ```res``` and count the number of times that the first names appear in ```oliver-twist.txt```. It then generates a txt file ```/output/count.txt``` containing the result in the format 
```
$name: $count
```

e.g.

```
Oliver: 4
James: 1
```

## Part 2

```bash
yarn run start
```

Starts a rest API server on port 5000

End point:
```
GET http://localhost:5000/name-count/:name
```
e.g. 
```
curl -XGET 'http://localhost:5000/name-count/Oliver'
```

Example response
```
{ "name": "Oliver", "count": 830 }
```

Example error response
```
{ 
    "code": "ValidationException",
    "message": "Invalid name", 
    "status": 400 
}
```

## Tests

```bash
yarn run test
```
