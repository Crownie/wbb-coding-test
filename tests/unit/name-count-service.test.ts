import NameCountService from '../../src/name-count.service';

jest.mock('fs');

const service = new NameCountService();
const fs = require('fs');

beforeAll(() => {
  fs.readFileSync.mockImplementation(() => {
    return 'Oliver Twist';
  });
});

describe('getNameCountFromOliverTwist', () => {

  it('calls getNameCountFromOliverTwist', () => {
    const out = service.getNameCountFromOliverTwist('Oliver');
    expect(out).toEqual({name: 'Oliver', count: 1});
  });

  it('throws validation exception if is invalid name', () => {
    let error;
    try {
      service.getNameCountFromOliverTwist('Ol1v3r')
    } catch (e) {
      error = e;
    }
    expect(error.code).toEqual('ValidationException');
  });
});

describe('getNameCountFromOliverTwist', () => {

  it('calls getNameCountFromOliverTwist', () => {
    const out = service.getNameCountFromOliverTwist('Oliver');
    expect(out).toEqual({name: 'Oliver', count: 1});
  });

  it('throws validation exception if is invalid name', () => {
    let error;
    try {
      service.getNameCountFromOliverTwist('Ol1v3r')
    } catch (e) {
      error = e;
    }
    expect(error.code).toEqual('ValidationException');
  });
});

describe('getNameCounts', function () {
  const oliverTwist = `Oliver's Twist, was sometimes known as Oliver, but his full name was Mr. Oliver James Twist. For the sake of ease we will just call him Oliver.`;

  it('counts occurrence of first name', () => {
    const firstNames = ['Oliver'];
    const [{name, count}] = service.getNameCounts(oliverTwist, firstNames);
    expect(name).toEqual(firstNames[0]);
    expect(count).toEqual(4);
  });

  it('should return 0 for names not found', () => {
    const firstNames = ['Tobi'];
    const [{name, count}] = service.getNameCounts(oliverTwist, firstNames);
    expect(name).toEqual(firstNames[0]);
    expect(count).toEqual(0);
  });

  it('should return zero for partial name', () => {
    const firstNames = ['Olive'];
    const [{name, count}] = service.getNameCounts(oliverTwist, firstNames);
    expect(name).toEqual(firstNames[0]);
    expect(count).toEqual(0);
  });

});
