import request from 'supertest';
import api from '../../src/api';

describe('name-count', () => {
  it('should return name count', async () => {
    const name = 'Oliver';
    const url = '/name-count/' + name;
    const res = await request(api)
      .get(url)
      .send();
    expect(res.body.count).toEqual(830);
    expect(res.body.name).toEqual(name);
  });

  it('should return 400 error if invalid name is supplied', async () => {
    const name = 'Ol1v3r';
    const url = '/name-count/' + name;
    const res = await request(api)
      .get(url)
      .send();
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual('ValidationException');
  });

  it('should return 404 if name is not specified', async () => {
    const name = '';
    const url = '/name-count/' + name;
    const res = await request(api)
      .get(url)
      .send();
    expect(res.status).toEqual(404);
  });
});
