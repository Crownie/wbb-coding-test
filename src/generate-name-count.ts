import fs from 'fs';
import NameCountService, {NameCount} from './name-count.service';

const service = new NameCountService();

const oliverTwistTxt = fs.readFileSync(__dirname + '/../res/oliver-twist.txt', 'utf8');
const firstNamesTxt = fs.readFileSync(__dirname + '/../res/first-names.txt', 'utf8');

let firstNames = firstNamesTxt.split(/[\n,\r]/);

let firstNamesCount: NameCount[] = service.getNameCounts(oliverTwistTxt, firstNames);

firstNamesCount = firstNamesCount.sort((a, b) => {
  return (a.count < b.count) ? 1 : -1;
});

const firstNamesCountTxt = firstNamesCount.map(({name, count}) => {
  return `${name}: ${count}`;
});

const output = firstNamesCountTxt.join('\n');

fs.writeFileSync(__dirname + '/../output/counts.txt', output);
