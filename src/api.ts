import express, {Application} from 'express';
import NameCountService from './name-count.service';

const api: Application = express();

api.get('/name-count/:name', (req, res) => {
  try {
    const service = new NameCountService();
    res.json(service.getNameCountFromOliverTwist(req.params.name));
  } catch (e) {
    if (e.code === 'ValidationException') {
      res.status(400).json(e);
    } else {
      res.status(500).json(e);
    }
  }
});

export default api;
