import fs from 'fs';

export default class NameCountService {

  getNameCountFromOliverTwist(name: string) {
    if (/^[A-z]+$/.test(name) === false) {
      throw {code: 'ValidationException'};
    }
    const oliverTwistTxt = fs.readFileSync(__dirname + '/../res/oliver-twist.txt', 'utf8');
    return this.getNameCounts(oliverTwistTxt, [name])[0];
  }

  getNameCounts(text: string, names: string[]): NameCount[] {
    const obj: any = {};
    for (let name of names) {
      obj[name] = 0;
    }
    const words = text.split(/[^A-Za-z]/);
    for (let word of words) {
      if (obj.hasOwnProperty(word)) {
        obj[word]++;
      }
    }
    const nameCounts = [];
    for (let name in obj) {
      nameCounts.push({name, count: obj[name]})
    }
    return nameCounts;
  }
}

export interface NameCount {
  name: string,
  count: number
}
